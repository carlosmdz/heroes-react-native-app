import React, {useRef} from 'react';
import {Animated, StyleSheet, View, Text} from 'react-native';
import colorPalette from '../styles/color-palette';

const ReviewHistory = props => {
  const {heroe, show} = props;
  const fadeAnim = useRef(new Animated.Value(0)).current;

  if (show) {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    }).start();
  } else {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true,
    }).start();
  }

  const renderRaitings = heroe.pastRatings.map((item, i) => (
    <View key={`hist-${heroe.id}-${i}`} style={styles.reviewsContainer}>
      <Text style={styles.reviewInfo}>{item.ratedBy}</Text>
      <Text style={styles.arrow}>{'➔'}</Text>
      <Text style={styles.reviewInfo}>{`${'🌟'.repeat(item.rating)}`}</Text>
    </View>
  ));

  return (
    <Animated.View
      style={[
        styles.animationContainer,
        {
          opacity: fadeAnim,
        },
      ]}>
      {show && (
        <>
          <Text style={styles.mainHeader}>Ratings History</Text>
          <View style={styles.reviewsHeaderContainer}>
            <Text style={styles.reviewsHeader}>Reviewer</Text>
            <Text style={styles.reviewsHeader}>Rating</Text>
          </View>
          {renderRaitings}
        </>
      )}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  animationContainer: {
    alignItems: 'center',
    marginTop: 5,
  },
  mainHeader: {
    fontWeight: 'bold',
    borderTopWidth: 0.5,
    borderTopColor: colorPalette.complementary,
    width: '100%',
    textAlign: 'center',
  },
  reviewsHeaderContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    marginBottom: 5,
  },
  reviewsContainer: {
    flexDirection: 'row',
    width: '85%',
    alignItems: 'center',
  },
  reviewInfo: {
    flex: 1,
    marginVertical: 2,
    fontSize: 12,
  },
  reviewsHeader: {
    flex: 1,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  arrow: {
    color: colorPalette.complementary,
    marginHorizontal: 10,
    fontSize: 15,
    fontWeight: 'bold',
  },
});

export default ReviewHistory;
