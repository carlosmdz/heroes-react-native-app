import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableNativeFeedback,
} from 'react-native';
import ReviewHistory from './ReviewHistory';
import colorPalette from '../styles/color-palette';

const HeroeItem = props => {
  const {heroe} = props;
  const [showHistory, setShowHistory] = useState(false);

  const switchInfoHandler = () => {
    setShowHistory(v => !v);
  };
  return (
    <TouchableNativeFeedback onPress={switchInfoHandler}>
      <View style={styles.container}>
        <View style={styles.mainInfo}>
          <Image style={styles.tinyLogo} source={{uri: heroe.picture}} />
          <View>
            <Text style={styles.mainText}>{heroe.name}</Text>
            <View style={styles.dataContainer}>
              <Text style={styles.strongText}>Age:</Text>
              <Text>{`${heroe.age} years old.`}</Text>
            </View>
            <View style={styles.dataContainer}>
              <Text style={styles.strongText}>City:</Text>
              <Text>{`${heroe.city}.`}</Text>
            </View>
            <View style={styles.dataContainer}>
              <Text style={styles.strongText}>Rating:</Text>
              <Text>{`${'🌟'.repeat(heroe.rating)}`}</Text>
            </View>
          </View>
        </View>
        <ReviewHistory show={showHistory} heroe={heroe} />
        <Text style={styles.triangle}>{showHistory ? '▲' : '▼'}</Text>
      </View>
    </TouchableNativeFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colorPalette.background,
    paddingHorizontal: 15,
    paddingTop: 15,
    paddingBottom: 2,
    marginHorizontal: 25,
    marginVertical: 15,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  mainInfo: {
    flexDirection: 'row',
  },
  dataContainer: {
    flexDirection: 'row',
    marginBottom: 2,
  },
  mainText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 3,
  },
  strongText: {
    fontWeight: 'bold',
    marginRight: 4,
  },
  tinyLogo: {
    width: 90,
    height: 90,
    borderRadius: 90 / 2,
    marginRight: 12,
  },
  triangle: {
    marginTop: 5,
    paddingTop: 2,
    color: colorPalette.complementary,
    textAlign: 'center',
    borderTopColor: colorPalette.complementary,
    borderTopWidth: 0.5,
    fontSize: 15,
    fontWeight: 'bold',
  },
});

export default HeroeItem;
