import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import HeroeItem from './HeroeItem';

const HeroesList = props => {
  const {heroes} = props;
  return (
    <View style={styles.container}>
      <FlatList
        data={heroes}
        keyExtractor={item => `h${item.id}`}
        renderItem={({item}) => {
          return <HeroeItem heroe={item} />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
});

export default HeroesList;
