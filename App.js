import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Home from './screens/Home';
import Heroes from './screens/Heroes';

import colorPalette from './styles/color-palette';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{...headerOptions, title: 'Heroes App'}}
        />
        <Stack.Screen
          name="Heroes"
          component={Heroes}
          options={{...headerOptions, title: 'Heroes List'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const headerOptions = {
  headerStyle: {
    backgroundColor: colorPalette.primary,
  },
  headerTintColor: colorPalette.headerText,
};

export default App;
