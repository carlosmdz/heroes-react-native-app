import React, {useState, useEffect} from 'react';
import {ActivityIndicator, StyleSheet, View, Text} from 'react-native';
import HeroesList from '../components/HeroesList';
import colorPalette from '../styles/color-palette';

const Heroes = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const [heroes, setHeroes] = useState([]);

  useEffect(() => {
    fetch('https://heroes-app-7324d.firebaseio.com/heroes.json')
      .then(response => {
        return response.json();
      })
      .then(data => {
        setHeroes(data);
        setIsLoading(false);
      })
      .catch(err => {
        console.log(err);
        setIsLoading(false);
        setHasError(true);
      });
  }, []);

  let renderContent = (
    <View style={styles.infoContainer}>
      <ActivityIndicator size="large" color={colorPalette.complementary} />
    </View>
  );
  if (hasError) {
    renderContent = (
      <View style={styles.infoContainer}>
        <Text style={styles.errorText}>Something went wrong</Text>
      </View>
    );
  } else if (!isLoading) {
    renderContent = <HeroesList heroes={heroes} />;
  }
  return <View style={styles.container}>{renderContent}</View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colorPalette.background,
  },
  infoContainer: {
    marginTop: 25,
  },
  errorText: {
    fontSize: 20,
  },
});

export default Heroes;
