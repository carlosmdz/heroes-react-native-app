import React from 'react';
import {StyleSheet, View, Text, Button} from 'react-native';
import colorPalette from '../styles/color-palette';

const Home = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={styles.text}>Welcome!</Text>
      </View>
      <View style={styles.buttonContainer}>
        <Button
          color={colorPalette.complementary}
          title="View Heroes List"
          onPress={() => navigation.navigate('Heroes')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorPalette.background,
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 2,
    width: '80%',
  },
  text: {
    color: 'black',
    textAlign: 'center',
    fontSize: 30,
    marginBottom: 40,
  },
});

export default Home;
