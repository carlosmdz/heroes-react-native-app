const colorPalette = {
  primary: '#51CBFB',
  complementary: '#FB8151',
  analogous1: '#51FBD6',
  analogous2: '#5176FB',
  tiradic1: '#8151FB',
  tiradic2: '#FB51CB',
  headerText: '#FFF',
  background: '#F3F3F3',
};
export default colorPalette;
